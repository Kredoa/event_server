const NIGHT_START = 22;
const NIGHT_END = 6;

module.exports = {
  NIGHT_START,
  NIGHT_END
}