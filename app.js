const express = require('express')
const bodyParser = require('body-parser')
const router = express.Router()
const routes = require('./app/api')(router, {})
// const postRoutes = require('./app/api/post')
// const authorRoutes = require('./app/api/author')

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true
}))

app.use(express.static('app/public'))

app.use('/api', routes)

app.get('/', (req, res) => {
  res.status(200).send('Hello.')
})

// postRoutes(app, db)
// authorRoutes(app, db)

module.exports = app