const add = require('date-fns/add')
// Overall, great work. I hadn't thought of the way you've solved the problem and I like it.
// Make sure there are enough assertions in the tests because here there is a bug you failed to see because of missing "expects"
// I really like the data flow you've used and the seperation of Event  from the EventConverter. stillisticly this is my favorite so far.

class Event {

  startDate;
  endDate;

  constructor(start, end) {
    start ? this.startDate = start : this.startDate = new Date();
    end ? this.endDate = end : this.endDate = add(this.startDate, { hours: 5});
  }
}

module.exports = Event;