const EventForPay = require("./EventForPay");
const eachHourOfInterval = require('date-fns/eachHourOfInterval')
const { NIGHT_START, NIGHT_END } = require('../constants/time')


class EventConverterForPay {

  event;

  constructor(event) {
    this.event = event;
  }

  /*
  return an array with all the EventForPay
   */
  getSlicedEvents(start, end) {
    let eventsForPay = [];

    //return all the hours from this.event
    const hours = eachHourOfInterval({
      start: start,
      end: end
    })

    let tmpStart = start;
    let tmpEnd = end;

    /*For each hour we check:
    - if the current hour is the final endDate of our event
    - if the current hour is the start of the night, so we closed an eventForPay (day)
    - if the current hour is the end of the night, so we closed an eventForPay (night)
     */

     /*
     This is an interesting way to solve the problem that I haven't thought of. good work.
     */
    hours.forEach((h) => {
      if (h.getHours() === end.getHours() && h.getDate() === end.getDate()) {
        eventsForPay.push(new EventForPay(tmpStart, end))
      } else if (h.getHours() === NIGHT_START) {
        tmpEnd = new Date(h.getYear(), h.getMonth(), h.getDate(), NIGHT_START-1,59);
        eventsForPay.push(new EventForPay(tmpStart, tmpEnd))
        tmpStart = h
      } else if (h.getHours() === NIGHT_END) {
        tmpEnd = new Date(h.getYear(), h.getMonth(), h.getDate(), NIGHT_END-1,59);
        eventsForPay.push(new EventForPay(tmpStart, tmpEnd))
        tmpStart = h
      }
    })

    return eventsForPay
  }

  to_hash() {
    let start = this.event.startDate;
    let end = this.event.endDate;

    return this.getSlicedEvents(start, end);
  }
}

module.exports = EventConverterForPay