const { Account } = require('../models')

class EventConverter {

  eventPayload;

  constructor(googlePayload) {
    this.eventPayload = googlePayload;
  }

  //get unique id from payload
  getUniqueId() {
    const url = new URL(this.eventPayload.html_link);
    return url.searchParams.get("eid");
  }

  //return start date from payload
  getStartDate() {
    return new Date(this.eventPayload.start.date_time)
  }

  //return end date from payload
  getEndDate() {
    return new Date(this.eventPayload.end.date_time)
  }

  //Filter all the participants of the payload
  getAttendees() {
    let all = this.eventPayload.attendees
    return all.filter((att) => !att.self)
  }

  async clients() {
    let account = await Account.findOne();
    let clients = await account.getClients()
    return this.eventPayload.attendees
      .map((attendee) => {
        if (clients.find((client) => client.email === attendee.email)) {
          return attendee;
        }
        return null;
      })
      .filter((attendee) => !!attendee);
  }

  async employees() {
    let account = await Account.findOne();
    let employees = await account.getEmployees()
    return this.eventPayload.attendees
      .map((attendee) => {
        if (employees.find((e) => e.email === attendee.email)) {
          return attendee;
        }
        return null;
      })
      .filter((attendee) => !!attendee);
  }

  //return the new body of the payload formatted with
  async getFormatedBody() {
    return {
      "attendees": this.getAttendees(),
      "clients": await this.clients(),
      "employees": await this.employees(),
      "end": {
        "date_time": this.getEndDate()
      },
      "id": this.getUniqueId(),
      "start": {
        "date_time": this.getStartDate()
      },
    }
  }
}

module.exports = EventConverter;