const { NIGHT_START } = require('../constants/time')

class EventForPay {

  startDate;
  endDate;
  price;

  constructor(start, end) {
    this.startDate = start;
    this.endDate = end;
    if(this.startDate.getHours() >= NIGHT_START) {
      this.price = 15
    } else {
      this.price = 8
    }
  }
}

module.exports = EventForPay;