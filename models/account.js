'use strict';
const {
  Model,
  Op
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Account extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Account.belongsTo(models.Company)
    }

    async getClients() {
      return await sequelize.models.User.findAll({
        where: {
          [Op.not]: {
            CompanyId: this.CompanyId
          }
        }
      })
    }

    async getEmployees() {
      return await sequelize.models.User.findAll({
        where: {
          CompanyId: this.CompanyId
        }
      })
    }
  };

  Account.init({
    name: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Account',
  });
  return Account;
};