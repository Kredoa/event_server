'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.renameColumn('Users', 
  'Password', 'password');
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.renameColumn('Users', 'Password', 'password');
  }
};
