const request = require('supertest')
const app = require('../app')
const db = require('../models');


describe('GET /', () => {
  let response;

  beforeEach(async () => {
    await cleanDb(db)
    response = await request(app).get('/');
  })

  test('It should respond with a 200 status code', async () => {
    expect(response.statusCode).toBe(200);
  });
});

describe('Account Tests', () => {
  let account;

  beforeEach(async () => {
    account = await db.Account.findOne();
  })

  test('get Account Clients', async () => {
    expect((await account.getClients()).length).toBe(2);
  });

  test('get Account Employees', async () => {
    expect((await account.getEmployees()).length).toBe(3);
  });
});

describe('Event Converter Tests', () => {
  let eventConverter = new EventConverter(googlePayload);

  test('getUID', async () => {
    expect(eventConverter.getUniqueId()).toBe("MGptdjJ1ZDljMWo3Y2kyZzFqZ21ybWY2c3Mgbmlja0BnZW1iYW5pLmNvbQ");
  });

  test('start date', async () => {
    let date = new Date(googlePayload.start.date_time)
    expect(eventConverter.getStartDate().toDateString()).toBe(date.toDateString())
  });

  test('end date', async () => {
    let date = new Date(googlePayload.end.date_time)
    expect(eventConverter.getEndDate().toDateString()).toBe(date.toDateString())
  })

  test('attendees filter', async () => {
    let attendees = [
      {
        "displayName": "Nick Stock",
        "email": "client@client.com",
        "organizer": true,
        "response_status": "accepted"
      }
    ];
    expect(eventConverter.getAttendees()).toEqual(attendees)
  })

  test('formated body', async () => {
    let newBody = {
      "attendees": [
        {
          "displayName": "Nick Stock",
          "email": "client@client.com",
          "organizer": true,
          "response_status": "accepted"
        }
      ],
      "clients": [],
      "employees": [],
      "end": {
        "date_time": new Date(googlePayload.end.date_time)
      },
      "id": "MGptdjJ1ZDljMWo3Y2kyZzFqZ21ybWY2c3Mgbmlja0BnZW1iYW5pLmNvbQ",
      "start": {
        "date_time": new Date(googlePayload.start.date_time)
      },
    }
    expect(await (eventConverter.getFormatedBody())).toEqual(newBody)
  })
})

const Event = require('../src/Event')
const EventConverterForPay = require('../src/EventConverterForPay')
const {NIGHT_START, NIGHT_END} = require("../constants/time");

// This should be in a different file from the previous tests. 
//I deleted them so that I didn't have to setup myself for the DB to work in order to run your current tests.
describe('Event For Pay', () => {

  let event;
  let eventConverterForPay;
  let eventsForPay = [];

  test('start at day, end at day in the same day', async () => {
    let start = new Date(2021, 3, 5, 10,0);
    let end = new Date(2021, 3, 5, 15,0);
    event = new Event(start, end);
    eventConverterForPay = new EventConverterForPay(event);
    eventsForPay = eventConverterForPay.to_hash();

    expect(eventsForPay.length).toBe(1)
    expect(eventsForPay[0].startDate).toEqual(start)
    expect(eventsForPay[0].endDate).toEqual(end)
    expect(eventsForPay[0].price).toEqual(8)
  })

  test('start at night, end at night in the same day', async () => {
    let start = new Date(2021, 3, 5, NIGHT_START+1,0);
    let end = new Date(2021, 3, 6, 5,0);
    event = new Event(start, end);
    eventConverterForPay = new EventConverterForPay(event);
    eventsForPay = eventConverterForPay.to_hash();

    expect(eventsForPay.length).toBe(1)
    expect(eventsForPay[0].startDate).toEqual(start)
    expect(eventsForPay[0].endDate).toEqual(end)
    expect(eventsForPay[0].price).toEqual(15)
  })

  test('start at day, end at night in the same day', async () => {
    let start = new Date(2021, 3, 5, 15, 0);
    let end = new Date(2021, 3, 5, 23, 0);
    event = new Event(start, end);
    eventConverterForPay = new EventConverterForPay(event);
    eventsForPay = eventConverterForPay.to_hash();

    expect(eventsForPay.length).toBe(2)
    expect(eventsForPay[0].startDate).toEqual(start)
    expect(eventsForPay[1].endDate).toEqual(end)
    expect(eventsForPay[0].price).toEqual(8)
    expect(eventsForPay[1].price).toEqual(15)
  })

  test('starts during the night ends during the day', async () => {
    let start = new Date(2021, 3, 5, NIGHT_START+1, 0);
    let end = new Date(2021, 3, 6, NIGHT_END+6, 0)
    event = new Event(start,end);
    eventConverterForPay = new EventConverterForPay(event);
    eventsForPay = eventConverterForPay.to_hash();

    expect(eventsForPay.length).toBe(2)
    expect(eventsForPay[0].startDate).toEqual(start)
    expect(eventsForPay[1].endDate).toEqual(end)
    expect(eventsForPay[0].price).toEqual(15)
    expect(eventsForPay[1].price).toEqual(8)
  })

  test('The event starts during the day, continue during the night and ends during the day', async () => {
    let start = new Date(2021, 3, 5, 15, 0);
    let end = new Date(2021, 3, 6, 13, 0);
    event = new Event(start,end);
    eventConverterForPay = new EventConverterForPay(event);
    eventsForPay = eventConverterForPay.to_hash();

    expect(eventsForPay.length).toBe(3)
    expect(eventsForPay[0].startDate).toEqual(start)
    expect(eventsForPay[2].endDate).toEqual(end)
    expect(eventsForPay[0].price).toEqual(8)
  })

  test('The event starts during the day, continue during the night and ends during the day - on 2 days', async () => {
    let start = new Date(2021, 3, 5, 15, 0);
    let end = new Date(2021, 3, 7, 13, 0);
    event = new Event(start,end);
    eventConverterForPay = new EventConverterForPay(event);
    eventsForPay = eventConverterForPay.to_hash();

    expect(eventsForPay.length).toBe(5)
    expect(eventsForPay[0].startDate).toEqual(start)
    expect(eventsForPay[4].endDate).toEqual(end)
  })

  test('The event starts during the day, continue during the night and ends during the day - on lot of days', async () => {
    let start = new Date(2021, 3, 5, 15, 0);
    let end = new Date(2021, 3, 28, 13, 0);
    event = new Event(start,end);
    eventConverterForPay = new EventConverterForPay(event);
    eventsForPay = eventConverterForPay.to_hash();

    console.log(eventsForPay)
    let nightsEvents = eventsForPay.filter((e) => e.price === 15)

    //23 nuits en tout donc 23 events de nuit
    expect(nightsEvents.length).toBe(23)
    expect(eventsForPay[0].startDate).toEqual(start)
    expect(eventsForPay[eventsForPay.length-1].endDate).toEqual(end)
  })  
})

describe('GET / USERS', () => {
  let response;
  beforeEach(async () => {
    response = await request(app).get('/users');
  })
  test('It should respond with a 200 status code', async () => {
    expect(response.statusCode).toBe(200);
  });
});

describe('POST / USERS', () => {
  let response;
  beforeEach(async () => {
    response = await (request(app).post('/users')).send({firstName : 'Quentin' , lastName : 'DUCHESNE', email : 'quentin@duchesne.fr' });
  })
  test('It should respond with a 200 status code', async () => {
    expect(response.statusCode).toBe(200);
  });
});

describe('GET /companies', () => {
    let response;
    beforeEach(async () => {
        response = await request(app).get('/companies');
    })

    test('get Companies', async () => {
        expect(response.statusCode).toBe(200);
    });
});

describe('POST /session/new', () => {
  let response;
  beforeEach(async () => {
    response = await request(app).post('/session/new').send({email: 'test@test.fr', password : 'testtest'})
  })

  test('The user is connected', async () => {
    expect(response.statusCode).toBe(200);
  });

  test('The user is not connected', async () => {
    expect(response.statusCode).toBe(200)
  });

})