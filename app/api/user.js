const db = require('../../models')

module.exports = (app) => {
  app.get('/users', async(req,res) => {
    const users = await db.User.findAll();
    res.json(users)
  })

  app.post('/users', async({body},res) => {
    const newUser = await db.User.create(body)
    res.json(newUser);
  })
}