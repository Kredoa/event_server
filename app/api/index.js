const user = require('./user')
const company = require('./company')
const auth = require('./auth')

module.exports = (app) => {
  user(app)
  company(app)
  auth(app)

  return app;
}