const db = require('../../models')

module.exports = (app) => {
  app.get('/companies', async (req, res) => {
    const companies = await db.Company.findAll({
      // attributes: ['id'],
    })
    res.json(companies)
  })
}