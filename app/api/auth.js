const db = require('../../models')
const jwt = require('jsonwebtoken');

module.exports = (app) => {
  app.post('/session/new', async({body},res) => {
    const user = await db.User.findOne({
      where: { email : body.data.attributes.email }
    });
    // Si on trouve bien un utilisateur
    if(user !== null){
      const response = {
        statusCode : '',
        authState : { token : '', expiresIn : null },
        user : null,
      }
      // On vérifie que le MDP correspond au compte lié à l'email et on créé un token que l'on renvoit
      if(user.password === body.data.attributes.password){

        const token = jwt.sign({ email : user.email, password : user.password }, process.env.secret, { expiresIn: 3600 })
        delete user.dataValues.password;
        res.status(200).send({
          statusCode : 200,
          authState : { token : token, expiresIn : 3600 },
          user : user
        })
        // On retourne une erreur de MDP
      }else{
        res.status(403).send({
          ...response,
          statusCode : 403,
          error : {
            message : 'The password does not match with the email' }
        })
      }
      // Si user n'existe pas on en créé un et on le retourne avec un token généré
    }else{
      const newUser = await db.User.create(body)
      const token = jwt.sign({ email : newUser.email, password : newUser.password }, process.env.secret, { expiresIn: 3600 })
      delete newUser.dataValues.password;
      res.status(200).send({
        statusCode : 200,
        authState : { token : token, expiresIn : 3600 },
        user : newUser
      })
    }
  })
}