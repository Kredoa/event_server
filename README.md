# Node tdd Google Calendar

## Members
- **DUCHESNE** *Quentin*
- **MIMAULT** *Quentin*
- **REDOIS** *Lucas*

## Init
```bash
npm install
```
## Create database
```bash
npx sequelize-cli db:create
```
## Migration
```bash
npx sequelize-cli db:migrate
```
## Seeds
```bash
npx sequelize-cli db:seed:all
```
## Tests
```bash
npm run test
```

# Authentication planification

POST /session/new => (
  { 
    statusCode: 000,
    authState : { 
      token: 'dnjbfezgoigeiorz', 
      expiresIn: (tomorrow - today) 
    },
    user : {
      USER
    }
    error : {
      message: 'toto'
    },
  }
)

si user existe : email adress 
   si mdp correspond 
        alors créer token
   sinon 
        alors error
sinon
    créer User et retourner token


