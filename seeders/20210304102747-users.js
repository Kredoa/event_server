'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const companiesTab = await queryInterface.sequelize.query(
      `SELECT id from COMPANIES;`
    );
    const companies = companiesTab[0]
    return queryInterface.bulkInsert('Users', [
      {
        firstName: 'John',
        lastName: 'Doe',
        email: 'client1@example.com',
        createdAt: new Date(),
        updatedAt: new Date(),
        CompanyId: companies[0].id
      },
      {
        firstName: 'John',
        lastName: 'Doe2',
        email: 'client2@example.com',
        createdAt: new Date(),
        updatedAt: new Date(),
        CompanyId: companies[0].id
      },
      {
        firstName: 'John',
        lastName: 'Doe3',
        email: 'client3@example.com',
        createdAt: new Date(),
        updatedAt: new Date(),
        CompanyId: companies[0].id,
        Password: 'root'
      },
      {
        firstName: 'John',
        lastName: 'Doe4',
        email: 'employee1@example.com',
        createdAt: new Date(),
        updatedAt: new Date(),
        CompanyId: companies[1].id
      },
      {
        firstName: 'John',
        lastName: 'Doe5',
        email: 'employee2@example.com',
        createdAt: new Date(),
        updatedAt: new Date(),
        CompanyId: companies[1].id
      }
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Users', null, {});
  }
};
