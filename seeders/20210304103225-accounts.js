'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const companies = await queryInterface.sequelize.query(
      `SELECT id from COMPANIES;`
    );

    return queryInterface.bulkInsert('Accounts', [{
      name: `Account-client`,
      createdAt: new Date(),
      updatedAt: new Date(),
      CompanyId: companies[0][0].id
    }]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Accounts', null, {});
  }
};
